const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'g1xsz3',
  e2e: {
    baseUrl: "http://localhost:3000/",
    defaultCommandTimeout: 6000,
    // retries: 2,
    experimentalStudio: true,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
