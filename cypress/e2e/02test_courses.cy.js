/// <reference types="cypress" />
import * as LoginPage from '../e2e/pages/login_page'
import * as HeaderPage from './pages/header_page'
import * as CoursePage from '../e2e/pages/course_page'
import * as Utils from '../support/utils'

describe.skip('Check courses functionality', ()=>{
    beforeEach(function (){
        Utils.visitPage('login')
    })

    it('admin user should see delete buttons', function() {
        LoginPage.emailField().type('admin');
        LoginPage.passwordField().type('admin');
        LoginPage.loginButton().click();
        cy.get('[href="/courses"]').click()
        CoursePage.deleteButton().should('be.visible')
    })

    it.skip('admin user should able to add new course', function() {
        LoginPage.emailField().type('admin')
        LoginPage.passwordField().type('admin')
        LoginPage.loginButton().click();
        cy.get('[href="/courses"]').click()
        CoursePage.newCourseInput().type('Selenium')
        CoursePage.addCourseButton().click().should('have.length',1)
        
    })

    it.skip('admin user should able to delete the course', function() {
        LoginPage.emailField().type('admin')
        LoginPage.passwordField().type('admin')
        LoginPage.loginButton().click();
        cy.get('[href="/courses"]').click()
        CoursePage.deleteButton().last().click()
        CoursePage.deleteButton().should('have.length',2)

    })

    it.skip('normal user should able see the course list', function() {
        LoginPage.emailField().type('user')
        LoginPage.passwordField().type('user')
        LoginPage.loginButton().click();
        cy.get('[href="/courses"]').click()
        cy.get('ul').should('have.length',1)



    })

})
