/// <reference types="cypress" />
import * as LoginPage from '../e2e/pages/login_page'
import * as HeaderPage from './pages/header_page'
import * as CoursePage from '../e2e/pages/course_page'
import * as Utils from '../support/utils'

describe('Test login functionalities', function(){
    
    beforeEach(function (){
        // cy.visit('/login')
        Utils.visitPage('login')
    })

    it('login with valid email id and password for normal user', function(){
        LoginPage.emailField().type('user')
        LoginPage.passwordField().type('user')
        LoginPage.loginButton().click()
        cy.url().should('eq','http://localhost:3000/')
        cy.url().should('contain','http://localhost:3000/')
        cy.url().should('include','http://localhost:3000/')
        HeaderPage.logoutButton().should('be.visible')
    })

    /* ==== Test Created with Cypress Studio ==== */
    it('login with valid email id and password for admin user', function() {
        /* ==== Generated with Cypress Studio ==== */
        LoginPage.emailField().clear();
        LoginPage.emailField().type('admin');
        LoginPage.passwordField().clear();
        LoginPage.passwordField().type('admin');
        LoginPage.loginButton().click();
        /* ==== End Cypress Studio ==== */
        cy.url().should('eq','http://localhost:3000/')
        HeaderPage.logoutButton().should('be.visible')
        cy.get('[href="/courses"]').click()
        CoursePage.deleteButton().should('be.visible')
    });
})