/// <reference types="cypress" />
import * as LoginPage from '../e2e/pages/login_page'
import * as HeaderPage from './pages/header_page'
import * as CoursePage from '../e2e/pages/course_page'
import * as Utils from '../support/utils'

describe('Check courses functionality', ()=>{
    beforeEach(function (){
        Utils.visitPage('login')
   
    })
    it('admin user should able to logout', function() {
        LoginPage.emailField().type('admin');
        LoginPage.passwordField().type('admin');
        LoginPage.loginButton().click();
        HeaderPage.logoutButton().should('be.visible')
        // HeaderPage.logoutButton().click()
    })

})
