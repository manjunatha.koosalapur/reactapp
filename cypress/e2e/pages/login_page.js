export const emailField = ()=> cy.get('[type="text"]')
export const passwordField = ()=> cy.get('[type="password"]')
export const loginButton = ()=> cy.get('[type="submit"]')
// export const loginButton = ()=> cy.get('[data-cy="login_button"]')

export const performLogin = (email,password) =>{
    emailField().type('email')
    passwordField().type('password')
    loginButton().click()
}