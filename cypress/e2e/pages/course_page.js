export const deleteButton = () => cy.get('[data-cy=delete-button]')
export const newCourseInput = () => cy.get('.add-course-input')
export const addCourseButton = () => cy.get('.add-course')